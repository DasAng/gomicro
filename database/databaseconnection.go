package database

type DatabaseConnection interface {

	Connect() error
	Reconnect(pollTimeInSeconds int)
	WaitForConnection()
	CreateDatabase(databaseName string) error
	CreateTable(databaseName string, tableName string) error
}
