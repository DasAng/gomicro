package database

import (
	r "github.com/dancannon/gorethink"
	"time"
	"gitlab.com/DasAng/gomicro/logger"
	"fmt"
)

type RethinkDBConnection struct {
	Session                    *r.Session
	Option                     *RethinkDbOption
	OnStartupDatabaseConnected chan bool
}

type RethinkDbOption struct {
	Address string
	DatabaseName string
}

// NewRethinkDbConnection instantiates a new database connection instance
// The parameter RethinkDbOption is an option instance where you can specify DB address
// as well as the name of the database.
// This method will return a pointer to an instance of RethinkDBConnection.
func NewRethinkDbConnection(option *RethinkDbOption) *RethinkDBConnection {
	var obj *RethinkDBConnection
	obj = &RethinkDBConnection{
		Option: &RethinkDbOption{Address:"localhost:28015"},
		Session: nil,
		OnStartupDatabaseConnected:make(chan bool),
	}

	if option != nil {
		*obj.Option = *option
	}

	return obj
}

func (connection *RethinkDBConnection) Connect() error {

	session,err := r.Connect(r.ConnectOpts{Address:connection.Option.Address,
		Database:connection.Option.DatabaseName})
	if err == nil {
		logger.Print("Connected to RethinkDB")
		connection.Session = session
		connection.OnStartupDatabaseConnected <- true
	}
	return err
}

func (connection *RethinkDBConnection) Reconnect(pollTimeInSeconds int) {

	logger.Print("Retry connecting to RethinkDB every %d seconds",pollTimeInSeconds)

	for {
		<-time.After(time.Duration(pollTimeInSeconds) * time.Second)
		err := connection.Connect()
		if err == nil {
			return
		}
	}
}

func (connection *RethinkDBConnection) Close() {
	if connection.Session != nil {
		connection.Session.Close()
	}
}

func (connection *RethinkDBConnection) WaitForConnection() {
	<- connection.OnStartupDatabaseConnected
}

func (connection *RethinkDBConnection) CreateDatabase(databaseName string) error {
	if connection.Session == nil {
		return fmt.Errorf("Not connected to database")
	}

	_, err := r.DBCreate(databaseName).RunWrite(connection.Session)
	return err;
}

func (connection *RethinkDBConnection) CreateTable(databaseName string, tableName string) error {
	if connection.Session == nil {
		return fmt.Errorf("Not connected to database")
	}

	_, err := r.DB(databaseName).TableCreate(tableName).RunWrite(connection.Session)
	return err

}