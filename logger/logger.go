package logger

import "fmt"

var LogManagerInstance *LogManager

func PrintF(message string, metadata interface{}) {
	LogManagerInstance.Infof(message,metadata)
}
func Errorf(message string, metadata interface{}) {
	LogManagerInstance.Errorf(message,metadata)
}

func Print(message string, a ...interface{}) {
	LogManagerInstance.Info(fmt.Sprintf(message,a...))
}

func Error(message string, a ...interface{}) {
	LogManagerInstance.Error(fmt.Sprintf(message,a...))
}