package gomicro

import (
	"gitlab.com/DasAng/gomicro/logger"
	"github.com/gorilla/mux"
	"net/http"
	"github.com/urfave/negroni"
)

type Service interface {
	// Get will allow you to setup a handler for a REST GET verb.
	//
	// url is the uri endpoint. f is the function to handle the incoming request.
	// handlers are variable list of middleware functions that must have this signature:
	//
	// func (rw http.ResponseWriter, r *http.Request, next http.HandlerFunc)
	Get(url string, f http.HandlerFunc, handlers ...func(http.ResponseWriter, *http.Request, http.HandlerFunc))

	// Post will allow you to setup a handler for a REST POST verb.
	//
	// url is the uri endpoint. f is the function to handle the incoming request.
	// handlers are variable list of middleware functions that must have this signature:
	//
	// func (rw http.ResponseWriter, r *http.Request, next http.HandlerFunc)
	Post(url string, f http.HandlerFunc, handlers ...func(http.ResponseWriter, *http.Request, http.HandlerFunc))

	// Initialize must be called prior to calling any other function of Microservice.
	Initialize()

	// Start will start a HTTP web server listening for incoming request at the address specified.
	// This function will return an error object if it fails to start the service otherwise nil is returned.
	Start(address string) error

	// AddMiddlewares adds the specified list of middlewares to all route endpoints
	//
	// handler is a middleware handler chain that must have this signature:
	//
	// func (http.ResponseWriter, *http.Request, http.HandlerFunc)
	AddMiddlewares(handlers ...func(http.ResponseWriter, *http.Request, http.HandlerFunc))
}


// Microservice is a struct that helps you create RESTFUL services.
//
// Example usage:
//
// service := Microservice { ServiceName: "TestService" }
//
// service.Initialize()
type Microservice struct {
	ServiceName string      // ServiceName is the name of this service.
	router      *mux.Router //router is a pointer to a Gorilla mux router.
	handler http.Handler // global handler
}

// Initialize must be called prior to calling any other function of Microservice.
func (m *Microservice) Initialize() {

	m.router = mux.NewRouter()
	m.handler = m.router

	logger.LogManagerInstance.ApplicationName = m.ServiceName
}

// Get will allow you to setup a handler for a REST GET verb.
//
// url is the uri endpoint. f is the function to handle the incoming request.
// handlers are variable list of middleware functions that must have this signature:
//
// func (rw http.ResponseWriter, r *http.Request, next http.HandlerFunc)
func (m *Microservice) Get(
url string,
f http.HandlerFunc,
handlers ...func(http.ResponseWriter, *http.Request, http.HandlerFunc)) {
	m.createRoute(url,"GET",f,handlers...)
}

// Post will allow you to setup a handler for a REST POST verb.
//
// url is the uri endpoint. f is the function to handle the incoming request.
// handlers are variable list of middleware functions that must have this signature:
//
// func (rw http.ResponseWriter, r *http.Request, next http.HandlerFunc)
func (m *Microservice) Post(url string,
f http.HandlerFunc,
handlers ...func(http.ResponseWriter, *http.Request, http.HandlerFunc)) {
	m.createRoute(url,"POST",f,handlers...)
}

// Creates a new route for the specified url endpoint
//
// url is the uri endpoint. method is one of the HTTP verbs for exmple: POST,GET,PUT,DELETE.
// f is the function to handle the incoming request.
// handlers are variable list of middleware functions that must have this signature:
//
// func (rw http.ResponseWriter, r *http.Request, next http.HandlerFunc)
func (m *Microservice) createRoute(url string,method string,f http.HandlerFunc,handlers ...func(http.ResponseWriter, *http.Request, http.HandlerFunc)) {
	subRouter := mux.NewRouter().Path(url).Subrouter().StrictSlash(true)
	subRouter.Methods(method).HandlerFunc(f)
	n := negroni.New()

	for _, handler := range handlers {
		n.Use(negroni.HandlerFunc(handler))
	}
	n.Use(negroni.Wrap(subRouter))

	m.router.Path(url).Handler(n)
}

// Start will start a HTTP web server listening for incoming request at the address specified.
// This function will return an error object if it fails to start the service otherwise nil is returned.
func (m *Microservice) Start(address string) error {

	err := http.ListenAndServe(address, m.handler)

	return err
}


// AddMiddlewares adds the specified list of middlewares to all route endpoints
//
// handlers is optional list of middlewares to add
func (m *Microservice) AddMiddlewares(handlers ...func(http.ResponseWriter, *http.Request, http.HandlerFunc)) {
	n := negroni.New()

	for _, handler := range handlers {
		n.Use(negroni.HandlerFunc(handler))
	}
	n.UseHandler(m.router)
	m.handler = n
}