package test

import (
	"testing"
	"gitlab.com/DasAng/gomicro"
	"gitlab.com/DasAng/gomicro/logger"
	"net/http"
	"fmt"
	"gitlab.com/DasAng/gomicro/middlewares"
)

func TestMicroservice(t *testing.T) {

	t.Log("Test started")

	var x string = "started"
	logger.Print("Test %s ",x)

	var service gomicro.Service
	service = &gomicro.Microservice{
		ServiceName: "TestService",
	}
	service.Initialize()

	middlewares.ApplyAuthenticationMiddleware("X75sE4tD",service)
	service.Get("/ping", Ping,MyMiddleware)
	service.Post("/create", Create,MyMiddleware)

	err := service.Start(":8000")
	if err != nil {
		logger.Error(fmt.Sprintf("Could not start service=%s",err.Error()))
		t.Fatalf("Test failed")
	}

	t.Log("Test success")
}

func Ping(res http.ResponseWriter, req *http.Request) {
	fmt.Println("Ping")
	res.WriteHeader(200)
}

func Create(res http.ResponseWriter, req *http.Request) {
	fmt.Println("Create")
	res.WriteHeader(200)
}

func MyMiddleware(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	fmt.Println("My middleware start")
	next(rw, r)
}

func GlobalMiddleware(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	fmt.Println("global middleware")
	next(rw, r)
}