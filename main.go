package gomicro

import "gitlab.com/DasAng/gomicro/logger"

func init() {
	logger.LogManagerInstance = &logger.LogManager{}
	logger.LogManagerInstance.Init()
}