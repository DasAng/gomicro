package middlewares

import (
	"net/http"
	jwt "github.com/dgrijalva/jwt-go"
	"fmt"
	"gitlab.com/DasAng/gomicro/logger"
	microhttp "gitlab.com/DasAng/gomicro/net"
	"gitlab.com/DasAng/gomicro"
)

type Authentication struct {
	Secret string
}

func (auth *Authentication) Apply(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	w.Header().Set("Content-Type", "application/json")
	clientIp := struct {
		ClientIp string
	}{
		r.RemoteAddr,
	}
	token, err := jwt.ParseFromRequest(r, func(token *jwt.Token) (interface{}, error) {
		return []byte(auth.Secret), nil
	})
	if err == nil && token.Valid {
		next(w, r)
	} else {
		if err != nil {
			logger.Errorf(fmt.Sprintf("Unauthorized access. %s", err.Error()), clientIp)
		} else {
			logger.Errorf(fmt.Sprintf("Unauthorized access. Jwt token is not valid"), clientIp)
		}

		microhttp.JsonResponse(http.StatusUnauthorized, "Unauthorized access", w)
	}
}

func ApplyAuthenticationMiddleware(secret string,service gomicro.Service) {
	auth := Authentication{ Secret: secret }
	service.AddMiddlewares(auth.Apply)
}