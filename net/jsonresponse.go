package net

import (
	"gitlab.com/DasAng/gomicro/model"
	"encoding/json"
	"net/http"
)

func JsonResponse(statusCode int, message string,w http.ResponseWriter) {

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(&model.ResponseModel{Message: message})
}

func JsonResponseModel(statusCode int, response interface{},w http.ResponseWriter) {

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(response)
}
