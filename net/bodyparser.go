package net

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func ParseBody(r *http.Request, v interface{}) error {

	if r.Body == nil {
		return fmt.Errorf("Body is missing")
	}

	decoder := json.NewDecoder(r.Body)
	decodeErr := decoder.Decode(&v)
	if decodeErr != nil || v == nil {
		return decodeErr
	}
	return nil
}

