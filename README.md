# INTRODUCTION #


# TROUBLESHOOTING #

There is a little catch that is good to know when testing http requests with middlewares applied. 

When testing for example a HTTP GET request on an endpoint and you have applied a middleware to that endpoint,
you might experience that the middleware is being called twice.

This is because when doing the HTTP GET request the browser/client will also make a request for /favicon and this
result in a catch all that will also try to call your middleware.

see https://groups.google.com/forum/#!topic/golang-nuts/1sgaQGpIILM
